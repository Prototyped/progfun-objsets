package objsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.MutableList

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body", 20))
    val set3 = set2.incl(new Tweet("b", "b body", 20))
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)
    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("filter: predicate that is never true results in an empty set") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets < 0)) === 0)
    }
  }

  test("filter: predicate that is always true results in an identical set") {
    new TestSets {
      val result = set5 filter (_ => true)
      assert(asSet(set5) === asSet(result))
    }
  }

  test("union: set4c and set4d") {
    new TestSets {
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("union: union of set with itself is itself") {
    new TestSets {
      assert(asSet(set5 union set5) === asSet(set5))
    }
  }

  test("union: union of set with superset is the superset") {
    new TestSets {
      assert(asSet(set4c union set5) === asSet(set5))
    }
  }

  test("union: union of empty set with itself is empty set") {
    new TestSets {
      assert(asSet(set1 union set1).isEmpty)
    }
  }

  test("union: union of overlapping sets is the superset") {
    new TestSets {
      assert(asSet(set4c union set4d) === asSet(set5))
      assert(size(set4c union set4d) < size(set4c) + size(set4d))
    }
  }

  test("union: is associative") {
    new TestSets {
      val setC = set1 incl c
      val setD = set1 incl d
      assert(asSet((set3 union setC) union setD) === asSet(set3 union (setC union setD)))
    }
  }

  test("union: union of disjoint sets has elements from both") {
    new TestSets {
      val setC = set1 incl c
      val setD = set1 incl d
      val setCD = set1 incl c incl d
      assert(asSet(setC union setD) === asSet(setCD))
      assert(size(setC union setD) === size(setC) + size(setD))
    }
  }

  test("descending: set5") {
    new TestSets {
      val trends = set5.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
      assert(trends.tail.head.user == "b" || trends.tail.head.user == "a")
      assert(trends.tail.tail.head.user === "d")
      assert(trends.tail.tail.tail.head.user === "c")
      assert(trends.tail.tail.tail.tail === Nil)
    }
  }

  test("tweetsMatchingKeywords: all match") {
    new TestSets {
      val bodyKeywords = List("body")
      val setMatchingKeyword =
        GoogleVsApple tweetsMatchingKeywords (set5, bodyKeywords)
      assert(asSet(setMatchingKeyword) === asSet(set5))
    }
  }

  test("tweetsMatchingKeywords: none match") {
    new TestSets {
      val nonMatchingKeyword = List("foo")
      val setNonMatchingKeyword =
        GoogleVsApple tweetsMatchingKeywords (set5, nonMatchingKeyword)
      assert(asSet(setNonMatchingKeyword) === asSet(set1))
    }
  }

  test("tweetsMatchingKeywords: single match") {
    new TestSets {
      val singleMatchingKeyword = List("d ")
      val setSingleMatchingKeyword =
        GoogleVsApple tweetsMatchingKeywords (set5, singleMatchingKeyword)
      assert(asSet(setSingleMatchingKeyword) === asSet(set1 incl d))
    }
  }

  test("tweetsMatchingKeywords: multiple matches (but not all)") {
    new TestSets {
      val twoMatchingKeywords = List("c ", "d ")
      val setTwoMatchingKeywords =
        GoogleVsApple tweetsMatchingKeywords (set5, twoMatchingKeywords)
      assert(asSet(setTwoMatchingKeywords) === asSet(set1 incl c incl d))
    }
  }

  test("mostRetweeted: set5") {
    new TestSets {
      assert(set5.mostRetweeted.retweets === 20)
    }
  }

  test("mostRetweeted: set1") {
    new TestSets {
      intercept[java.util.NoSuchElementException] {
        set1.mostRetweeted
      }
    }
  }

  test("mostRetweeted: set5 without a") {
    new TestSets {
      val set5WithoutMostRetweeted = set5 remove set5.mostRetweeted
      assert(set5WithoutMostRetweeted.mostRetweeted.retweets === 20)
    }
  }

  test("mostRetweeted: set5 without a or b") {
    new TestSets {
      val set5WithoutMostRetweeted = set5 remove (set5.mostRetweeted)
      val set5WithoutAOrB =
        set5WithoutMostRetweeted remove (set5WithoutMostRetweeted.mostRetweeted)
      assert(set5WithoutAOrB.mostRetweeted.retweets === 9)
    }
  }

  test("TweetReader.allTweets terminates") {
    val allTweets = TweetReader.allTweets
    assert(!asSet(allTweets).isEmpty)
  }

  test("mostRetweeted: google") {
    val google = GoogleVsApple.googleTweets
    val highest = google.mostRetweeted
    assert(highest.retweets > google.remove(highest).mostRetweeted.retweets)
  }

  test("mostRetweeted: apple") {
    val apple = GoogleVsApple.appleTweets
    val highest = apple.mostRetweeted
    assert(highest.retweets > apple.remove(highest).mostRetweeted.retweets)
  }

  def asList(tweets: TweetList) = {
    val acc = MutableList[Tweet]()
    tweets foreach (tw => acc += tw)
    acc
  }

  test("descending: google") {
    assert(asList(GoogleVsApple.googleTweets.descendingByRetweet) forall(
      tw => GoogleVsApple.google.exists(tw.text contains _)))
  }

  test("descending: apple") {
    assert(asList(GoogleVsApple.appleTweets.descendingByRetweet) forall(
      tw => GoogleVsApple.apple exists (tw.text contains _)))
  }

  test("trending: google and apple") {
    assert(asList(GoogleVsApple.trending) forall(
      tw => GoogleVsApple.google.exists (tw.text contains _) ||
            GoogleVsApple.apple.exists (tw.text contains _)))
  }
}
